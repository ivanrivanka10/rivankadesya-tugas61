import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Image,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from '../utils/service/url';
import {useEffect} from 'react';
import {ActivityIndicator} from 'react-native';
import {convertCurrency} from '../utils/helpers';
import {deleteData, editData} from '../Controller/CRUD';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import apiProvider from '../utils/service/apiProvider';
import Carousel from 'react-native-snap-carousel';
import {Dimensions} from 'react-native';

const DetailScreen = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [unitImage, setUnitImage] = useState('');
  var dataMobil = route.params;
  const [isLoading, setisLoading] = useState(false);
  const [isModalOpen, setisModalOpen] = useState(false);
  const [isEmpty, setisEmpty] = useState(false);
  const [listData, setListData] = useState([]);
  const sliderWidth = Dimensions.get('window').width;
  const itemWidth = 280 + 20 * 2;
  useEffect(() => {
    getDataMobil();
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
      setUnitImage(data.unitImage);
    }
  }, []);
  const getDataMobil = async () => {
    const response = await apiProvider.getData();

    if (response?.items?.length) {
      setListData(response.items);
    }
  };
  console.log('apaa', listData);
  const renderItem = ({item}) => {
    return (
      <View>
        <TouchableOpacity
          style={styles.cardContainer}
          onPress={() => {
            setNamaMobil(item.title);
            setTotalKM(item.totalKM);
            setHargaMobil(item.harga);
            setUnitImage(item.unitImage);
          }}>
          <Image
            source={{
              uri: typeof item?.unitImage === 'string' ? item.unitImage : '',
            }}
            style={styles.cardImage}
          />
          <Text style={styles.cardTitle}>{item.title}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: 'yellow'}}>
      <View
        style={{
          // height: 316,
          flexDirection: 'row',
          zIndex: 3,
          // width: '100%',

          // borderRadius: 20,
          // marginTop: -300,
          position: 'absolute',
          // backgroundColor: 'white',
          top: 20,
          left: 20,
        }}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <AntDesign
            name="arrowleft"
            color={'white'}
            size={20}
            style={{padding: 25}}
          />
        </TouchableOpacity>
      </View>

      <ScrollView style={{backgroundColor: 'aqua'}}>
        {/* <View style={{}}> */}
        <Image source={{uri: unitImage}} style={{height: 200}} />
        {/* </View> */}
        <View
          style={{
            borderTopEndRadius: 30,

            height: 900,
            backgroundColor: '#f8f8f4',
            borderTopLeftRadius: 30,
            marginTop: -30,
          }}>
          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: '700',
              marginLeft: 32,
              marginTop: 24,
            }}>
            {namaMobil}
          </Text>
          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: '700',
              marginLeft: 32,
              marginTop: 24,
            }}>
            {convertCurrency(hargaMobil, 'Rp. ')}
          </Text>
          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: '700',
              marginLeft: 32,
              marginTop: 24,
            }}>
            Jarak Tempuh {totalKM} KM
          </Text>

          <TouchableOpacity
            style={[styles.btnAdd, {backgroundColor: 'green'}]}
            onPress={() => setisModalOpen(!isModalOpen)}>
            <Text style={{color: '#fff', fontWeight: '600'}}>Ubah Data</Text>
          </TouchableOpacity>
          {dataMobil ? (
            <TouchableOpacity
              style={[styles.btnAdd, {backgroundColor: 'red'}]}
              onPress={async () => {
                const response = await apiProvider.deleteData(dataMobil);
                navigation.pop();
              }}>
              <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
            </TouchableOpacity>
          ) : null}

          <View style={{marginTop: 20, height: 300}}>
            <Carousel
              layout={'default'}
              activeAnimationOptions={200}
              activeAnimationType="spring"
              data={listData}
              renderItem={renderItem}
              sliderWidth={sliderWidth}
              itemWidth={200}
              loop
              autoplay
            />
          </View>
        </View>
      </ScrollView>

      <Modal visible={isModalOpen} transparent={true}>
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,0.2)',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              width: '75%',
              height: 430,
              alignSelf: 'center',
              marginTop: 160,
              borderRadius: 10,
              paddingVertical: 30,
              paddingHorizontal: 20,
            }}>
            <TouchableOpacity onPress={() => setisModalOpen(false)}>
              <AntDesign
                name="close"
                size={20}
                style={{right: 0, position: 'absolute', marginTop: -10}}
              />
            </TouchableOpacity>

            <Text style={styles.titleinput}>Merk Mobil</Text>
            <TextInput
              value={namaMobil}
              onChangeText={text => {
                setNamaMobil(text);
              }}
              placeholder="Masukkan Merk Mobil"
              style={styles.txtInput}
            />
            <Text style={styles.titleinput}>Harga Mobil</Text>
            <Text style={styles.titleinput}>
              {convertCurrency(hargaMobil, 'Rp. ')}
            </Text>
            <TextInput
              value={hargaMobil}
              onChangeText={text => setHargaMobil(text)}
              placeholder="Masukkan Harga Mobil"
              style={styles.txtInput}
              keyboardType="number-pad"
            />
            <Text style={styles.titleinput}>Jarak Tempuh Mobil</Text>

            <TextInput
              value={totalKM}
              onChangeText={text => setTotalKM(text)}
              placeholder="Masukkan Jarak Tempuh Mobil"
              style={styles.txtInput}
              keyboardType="number-pad"
            />

            <TouchableOpacity
              style={styles.btnAdd}
              onPress={async () => {
                const params = {
                  _uuid: dataMobil._uuid,
                  title: namaMobil,
                  harga: hargaMobil,
                  totalKM,
                  unitImage:
                    'https://assets.mitsubishi-motors.co.id/articles/1638196306-foto-lead-1-minjpg.jpg',
                };
                if (!namaMobil) {
                  return alert('data mobil perlu diisi');
                }
                if (!hargaMobil) {
                  return alert('data mobil perlu diisi');
                }
                if (!totalKM) {
                  return alert('data mobil perlu diisi');
                }
                if (hargaMobil < 100000000) {
                  return alert('Harga Mobil Kurang dari 100 jt');
                }

                const response = await apiProvider.updateData(params);
                alert('berhasil');
                navigation.navigate('Home');
              }}>
              <Text style={{color: '#fff', fontWeight: '600'}}>
                {dataMobil ? 'Ubah Data' : 'Tambah Data'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};
const styles = StyleSheet.create({
  titleinput: {marginVertical: 10, fontSize: 14, fontWeight: '500'},
  btnAdd: {
    marginTop: 20,
    width: '50%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    alignSelf: 'center',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
  cardContainer: {
    backgroundColor: 'white',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
  cardImage: {
    width: 150,
    height: 150,
    borderRadius: 20,
    marginTop: 5,
    marginBottom: 10,
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default DetailScreen;
