import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  RefreshControl,
  View,
} from 'react-native';
import {useState} from 'react';
import {useEffect} from 'react';
import {Modal} from 'react-native';
import {BASE_URL, TOKEN} from '../utils/service/url';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import {useIsFocused} from '@react-navigation/native';
import {convertCurrency} from '../utils/helpers';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Axios from 'axios';
import apiProvider from '../utils/service/apiProvider';

export default function Home({navigation}) {
  const isFocused = useIsFocused();
  const [isModalOpen, setisModalOpen] = useState(false);
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [unitImage, setUnitImage] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  const [dataMobil, setDataMobil] = useState([]);
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  }, []);

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);
  const getDataMobil = async () => {
    const response = await apiProvider.getData();

    if (response?.items?.length) {
      setDataMobil(response.items);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Modal visible={isModalOpen} transparent={true}>
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,0.2)',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              width: '75%',
              height: 430,
              alignSelf: 'center',
              marginTop: 160,
              borderRadius: 10,
              paddingVertical: 30,
              paddingHorizontal: 20,
            }}>
            <TouchableOpacity onPress={() => setisModalOpen(false)}>
              <AntDesign
                name="close"
                size={20}
                style={{right: 0, position: 'absolute', marginTop: -10}}
              />
            </TouchableOpacity>

            <Text style={styles.titleinput}>Merk Mobil</Text>
            <TextInput
              onChangeText={text => {
                setNamaMobil(text);
              }}
              placeholder="Masukkan Merk Mobil"
              style={styles.txtInput}
            />
            <Text style={styles.titleinput}>Harga Mobil</Text>

            <Text style={styles.titleinput}>
              {convertCurrency(hargaMobil, 'Rp. ')}
            </Text>
            <TextInput
              onChangeText={text => setHargaMobil(text)}
              placeholder="Masukkan Harga Mobil"
              style={styles.txtInput}
              keyboardType="number-pad"
            />
            <Text style={styles.titleinput}>Jarak Tempuh Mobil</Text>
            <TextInput
              onChangeText={text => setTotalKM(text)}
              placeholder="Masukkan Jarak Tempuh Mobil"
              style={styles.txtInput}
              keyboardType="number-pad"
            />

            <TouchableOpacity
              style={styles.btnAdd}
              onPress={async () => {
                const params = {
                  title: namaMobil,
                  harga: hargaMobil,
                  totalKM,
                  unitImage:
                    'https://assets.mitsubishi-motors.co.id/articles/1638196306-foto-lead-1-minjpg.jpg',
                };
                if (!namaMobil) {
                  return alert('data nama mobil perlu diisi');
                }
                if (!hargaMobil) {
                  return alert('data harga mobil perlu diisi');
                }
                if (!totalKM) {
                  return alert('data kilometer mobil perlu diisi');
                }
                if (hargaMobil < 100000000) {
                  return alert('Harga Mobil Kurang dari 100 jt');
                }

                const update = await apiProvider.postData(params);

                setisModalOpen(false);
                getDataMobil();
                navigation.navigate('Home');
              }}>
              <Text style={{color: '#fff', fontWeight: '600'}}>
                Tambah Data
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      <FlatList
        data={dataMobil}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('DetailScreen', item)}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{width: '90%', height: 100, resizeMode: 'contain'}}
                source={{
                  uri:
                    typeof item?.unitImage === 'string' ? item.unitImage : '',
                }}
              />
            </View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item?.title ?? 'Data Kosong'}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {convertCurrency(item.harga, 'Rp. ')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => setisModalOpen(true)}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  titleinput: {marginVertical: 10, fontSize: 14, fontWeight: '500'},
  btnAdd: {
    marginTop: 20,
    width: '50%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    alignSelf: 'center',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});
