import axios from 'axios';
import {BASE_URL, TOKEN} from './url';

const API = async (
  url,
  Options = {
    method: 'GET',
    body: {},
    head: {},
  },
) => {
  const request = {
    baseURL: BASE_URL,
    method: Options.method,
    timeout: 10000,
    url,
    headers: Options.head,
    responseType: 'json',
  };
  if (
    request.method === 'POST' ||
    request.method === 'PUT' ||
    request.method === 'DELETE'
  ) {
    request.data = Options.body;
  }

  const res = await axios(request);

  if (res.status == 200) {
    return res.data;
  } else {
    return res;
  }
};

export default {
  getData: async () => {
    return API('mobil', {
      method: 'GET',
      head: {
        'Centent-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },

  postData: async params => {
    return API('mobil', {
      method: 'POST',
      body: [params],
      head: {
        'Centent-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },
  updateData: async params => {
    return API('mobil', {
      method: 'PUT',
      body: [params],
      head: {
        'Centent-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },
  deleteData: async params => {
    return API('mobil', {
      method: 'DELETE',
      body: [params],
      head: {
        'Centent-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        alert('data berhasil di ubah');
        return response;
      })
      .catch(err => {
        return err;
      });
  },
};
